package com.brunoapps.viacard.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ViaVerdeAPI {

    public static final String LOGIN = "https://gestao.viacard.pt/login";

    public static String getLoginToken(String html) {
        if(html != null) {
            Pattern p = Pattern.compile("<input type=\"hidden\" name=\"_token\" value=\"([a-z|A-Z|0-9]*)\">");
            Matcher m = p.matcher(html);

            if (m.find()) {
                // we're only looking for one group, so get it
                return m.group(1);
            }
        }

        return null;
    }
}
