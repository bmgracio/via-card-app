package com.brunoapps.viacard;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.brunoapps.viacard.helper.ViaVerdeAPI;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private OkHttpClient httpClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        httpClient = new OkHttpClient;

        final Button button = (Button) findViewById(R.id.loginButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                final EditText username = (EditText) findViewById(R.id.username);
                final EditText password = (EditText) findViewById(R.id.password);

                String usernameStr = username.getText().toString();
                String passwordStr = password.getText().toString();

                getToken(usernameStr, passwordStr);
            }
        });
    }

    protected void getToken(final String username, final String password){
        final Request request = new Request.Builder()
                .url(ViaVerdeAPI.LOGIN)
                .build();

        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, e.getMessage());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.e(TAG, "GetToken response failed: " + response.toString());
                } else {
                    String token = ViaVerdeAPI.getLoginToken(response.body().string());
                    login(username, password, token);
                }
            }
        });
    }

    protected void login(final String username, final String password, final String token){
        RequestBody emptyBody = RequestBody.create(null, new byte[0]);

        Request request = new Request.Builder()
                .url(ViaVerdeAPI.LOGIN + "?nif=247600466&password=43782527&_token=" + token)
                .post(emptyBody)
                .build();

        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, e.getMessage());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.e(TAG, "Login response failed: " + response.toString());
                } else {
                    Log.d(TAG, response.body().string());
                }
            }
        });
    }
}
